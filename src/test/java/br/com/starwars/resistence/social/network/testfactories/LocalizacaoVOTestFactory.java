package br.com.starwars.resistence.social.network.testfactories;

import br.com.starwars.resistence.social.network.vos.LocalizacaoVO;
import org.apache.tomcat.jni.Local;

public class LocalizacaoVOTestFactory {

    private LocalizacaoVOTestFactory(){

    }

    public static LocalizacaoVO localizacaoVOInstance(){
        LocalizacaoVO localizacaoVO = new LocalizacaoVO();
        localizacaoVO.setNome("Teste Localizacao");
        localizacaoVO.setLongitude(10.0);
        localizacaoVO.setLatitude(-10.0);
        return localizacaoVO;
    }

}
