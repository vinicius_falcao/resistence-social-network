package br.com.starwars.resistence.social.network.controllers;

import br.com.starwars.resistence.social.network.entities.ItemEntity;
import br.com.starwars.resistence.social.network.repositories.ItemRepository;
import br.com.starwars.resistence.social.network.repositories.RebeldeRepository;
import br.com.starwars.resistence.social.network.testfactories.CreateRebeldeVOTestFactory;
import br.com.starwars.resistence.social.network.vos.CreateRebeldeVO;
import br.com.starwars.resistence.social.network.vos.LocalizacaoVO;
import br.com.starwars.resistence.social.network.vos.UpdateLocalizacaoVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class RebeldeControllerTest {

    @Autowired
    private RebeldeController rebeldeController;
    @Autowired
    private RebeldeRepository rebeldeRepository;
    @Autowired
    private ItemRepository itemRepository;

    @Test
    public void shouldCreateNewRebelde(){
        final var createRebeldeVO = CreateRebeldeVOTestFactory.createRebeldeVOInstance();
        persistItemEntity(createRebeldeVO.getInventario().get(0).getIdItem());
        final var result = rebeldeController.save(createRebeldeVO);
        createRebeldeVO.setId(result.getId());

        assertThat(result).isEqualTo(createRebeldeVO);
    }

    @Test
    public void shouldUpdateLocalizacao(){
        final var rebeldeVO = CreateRebeldeVOTestFactory.createRebeldeVOInstance();
        persistItemEntity(rebeldeVO.getInventario().get(0).getIdItem());
        final var createdRebelde = rebeldeController.save(rebeldeVO);
        //New localizacao
        final var localizacaoVO = new LocalizacaoVO();
        localizacaoVO.setNome("Novo Nome");
        localizacaoVO.setLatitude(123.20);
        localizacaoVO.setLongitude(-54.234);
        final var novaLocalizacao = new UpdateLocalizacaoVO();
        novaLocalizacao.setIdRebelde(createdRebelde.getId());
        novaLocalizacao.setLocalizacao(localizacaoVO);
        rebeldeController.updateLocalizacao(novaLocalizacao);
        final var result = rebeldeController.findById(createdRebelde.getId());

        assertThat(result.getLocalizacao()).isEqualTo(localizacaoVO);
    }


    private CreateRebeldeVO aCreateRebeldeVO() {
        final var rebeldeVO = CreateRebeldeVOTestFactory.createRebeldeVOInstance();
        persistItemEntity(rebeldeVO.getInventario().get(0).getIdItem());
        return rebeldeController.save(rebeldeVO);
    }


    private void persistItemEntity(final Long itemId) {
        this.itemRepository.save(new ItemEntity(itemId, "Item Teste", 2l));
    }

}