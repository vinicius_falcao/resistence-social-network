package br.com.starwars.resistence.social.network.testfactories;

import br.com.starwars.resistence.social.network.vos.ItemInventarioVO;

public class ItemInventarioVOTestFactory {

    private ItemInventarioVOTestFactory(){

    }

    public static ItemInventarioVO itemInventarioVOInstance() {
        ItemInventarioVO itemInventarioVO = new ItemInventarioVO();
        itemInventarioVO.setIdItem(1L);
        itemInventarioVO.setQuantidade(2L);
        return itemInventarioVO;
    }
}
