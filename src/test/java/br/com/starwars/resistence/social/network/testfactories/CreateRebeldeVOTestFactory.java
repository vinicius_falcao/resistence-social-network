package br.com.starwars.resistence.social.network.testfactories;

import br.com.starwars.resistence.social.network.vos.CreateRebeldeVO;

import java.util.Arrays;

public class CreateRebeldeVOTestFactory {

    private CreateRebeldeVOTestFactory(){}

    public static CreateRebeldeVO createRebeldeVOInstance(){
        CreateRebeldeVO createRebeldeVO = new CreateRebeldeVO();
        createRebeldeVO.setLocalizacao(LocalizacaoVOTestFactory.localizacaoVOInstance());
        createRebeldeVO.setInventario(Arrays.asList(
                ItemInventarioVOTestFactory.itemInventarioVOInstance()
        ));
        createRebeldeVO.setIdade(25);
        createRebeldeVO.setGenero("Genero Teste");
        createRebeldeVO.setNome("Rebelde Teste");
        return createRebeldeVO;
    }

}
