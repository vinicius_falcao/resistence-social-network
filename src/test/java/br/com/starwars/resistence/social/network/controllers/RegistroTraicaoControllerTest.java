package br.com.starwars.resistence.social.network.controllers;

import br.com.starwars.resistence.social.network.entities.ItemEntity;
import br.com.starwars.resistence.social.network.repositories.ItemRepository;
import br.com.starwars.resistence.social.network.repositories.RegistroTraicaoRepository;
import br.com.starwars.resistence.social.network.testfactories.CreateRebeldeVOTestFactory;
import br.com.starwars.resistence.social.network.vos.CreateRebeldeVO;
import br.com.starwars.resistence.social.network.vos.ReportarTraidorVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class RegistroTraicaoControllerTest {

    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private RegistroTraicaoController registroTraicaoController;
    @Autowired
    private RebeldeController rebeldeController;
    @Autowired
    private RegistroTraicaoRepository registroTraicaoRepository;


    @Test
    public void shouldReportRebeldeAsTraidor(){
        final var relator = aCreateRebeldeVO();
        var reportado = aCreateRebeldeVO();

        final var report = new ReportarTraidorVO(relator.getId(), reportado.getId());
        registroTraicaoController.reportarTraicao(report);
        final var traicoes = registroTraicaoRepository.findByReportadoId(reportado.getId());
        assertThat(traicoes).hasSize(1);
        reportado = rebeldeController.findById(reportado.getId());
        assertThat(reportado.isTraidor()).isFalse();
    }

    @Test
    public void shouldReportRebeldeAsTraidorThreeTimes(){
        var reportado = aCreateRebeldeVO();

        reportarTraidor(reportado);
        reportarTraidor(reportado);
        reportarTraidor(reportado);

        final var traicoes = registroTraicaoRepository.findByReportadoId(reportado.getId());
        assertThat(traicoes).hasSize(3);
        reportado = rebeldeController.findById(reportado.getId());
        assertThat(reportado.isTraidor()).isTrue();
    }

    private void reportarTraidor(CreateRebeldeVO reportado) {
        final var relator1 = aCreateRebeldeVO();
        final var report1 = new ReportarTraidorVO(relator1.getId(), reportado.getId());
        registroTraicaoController.reportarTraicao(report1);
    }


    private CreateRebeldeVO aCreateRebeldeVO() {
        final var rebeldeVO = CreateRebeldeVOTestFactory.createRebeldeVOInstance();
        persistItemEntity(rebeldeVO.getInventario().get(0).getIdItem());
        return rebeldeController.save(rebeldeVO);
    }

    private void persistItemEntity(final Long itemId) {
        this.itemRepository.save(new ItemEntity(itemId, "Item Teste", 2l));
    }



}
