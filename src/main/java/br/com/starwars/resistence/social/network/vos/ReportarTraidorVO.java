package br.com.starwars.resistence.social.network.vos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReportarTraidorVO {

    private Long idRebeldeRelator;
    private Long idRebeldeReportado;
}
