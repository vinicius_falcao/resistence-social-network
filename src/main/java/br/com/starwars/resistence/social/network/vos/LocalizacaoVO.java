package br.com.starwars.resistence.social.network.vos;

import lombok.Data;

@Data
public class LocalizacaoVO {

    private String nome;
    private Double latitude;
    private Double longitude;

}
