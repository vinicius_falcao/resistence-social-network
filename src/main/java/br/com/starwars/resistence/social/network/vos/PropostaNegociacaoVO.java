package br.com.starwars.resistence.social.network.vos;

import lombok.Data;

import java.util.List;

@Data
public class PropostaNegociacaoVO {

    private Long idRebelde;
    private List<ItemInventarioVO> itemsInventario;

}
