package br.com.starwars.resistence.social.network.exceptions;

public class RebeldeNotFound extends RuntimeException{

    public RebeldeNotFound() {
        super("Rebelde not found exception");
    }
}
