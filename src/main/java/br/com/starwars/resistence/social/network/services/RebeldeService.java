package br.com.starwars.resistence.social.network.services;

import br.com.starwars.resistence.social.network.entities.LocalizacaoRebeldeEntity;
import br.com.starwars.resistence.social.network.entities.RebeldeEntity;
import br.com.starwars.resistence.social.network.vos.PropostaNegociacaoVO;

import java.util.List;

public interface RebeldeService {

    RebeldeEntity save(RebeldeEntity rebelde);
    List<RebeldeEntity> findAll();

    RebeldeEntity findById(Long id);

    void updateLocalizacao(Long idRebelde, LocalizacaoRebeldeEntity localizacaRebelde);

    void negociar(PropostaNegociacaoVO proposta1, PropostaNegociacaoVO proposta2);
}
