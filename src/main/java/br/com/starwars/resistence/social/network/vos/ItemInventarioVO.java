package br.com.starwars.resistence.social.network.vos;

import lombok.Data;

@Data
public class ItemInventarioVO {

    private Long idItem;
    private Long quantidade;

}
