package br.com.starwars.resistence.social.network.vos;

import lombok.Data;

@Data
public class UpdateLocalizacaoVO {

    private Long idRebelde;
    private LocalizacaoVO localizacao;
}
