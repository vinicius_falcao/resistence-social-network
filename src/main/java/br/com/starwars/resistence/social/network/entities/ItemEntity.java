package br.com.starwars.resistence.social.network.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemEntity {

    public ItemEntity(final Long id){
        this.id = id;
    }

    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    private Long valor;
}
