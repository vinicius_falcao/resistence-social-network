package br.com.starwars.resistence.social.network.repositories;

import br.com.starwars.resistence.social.network.entities.ItemInventarioEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemInventarioRepository extends JpaRepository<ItemInventarioEntity, Long> {
}
