package br.com.starwars.resistence.social.network.facades;

import br.com.starwars.resistence.social.network.vos.ReportarTraidorVO;

public interface RegistroTraicaoFacade {
    void reportarTraidor(ReportarTraidorVO reportarTraidorVO);
}
