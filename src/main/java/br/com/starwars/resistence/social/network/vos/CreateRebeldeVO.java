package br.com.starwars.resistence.social.network.vos;

import lombok.Data;

import java.util.List;

@Data
public class CreateRebeldeVO {

    private Long id;
    private String nome;
    private Integer idade;
    private String genero;
    private boolean traidor;
    private LocalizacaoVO localizacao;
    private List<ItemInventarioVO> inventario;

}
