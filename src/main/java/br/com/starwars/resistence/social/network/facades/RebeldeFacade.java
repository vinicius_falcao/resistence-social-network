package br.com.starwars.resistence.social.network.facades;

import br.com.starwars.resistence.social.network.vos.CreateRebeldeVO;
import br.com.starwars.resistence.social.network.vos.NegociacaoVO;
import br.com.starwars.resistence.social.network.vos.UpdateLocalizacaoVO;

import java.util.List;

public interface RebeldeFacade {

    CreateRebeldeVO save(CreateRebeldeVO createRebeldeVO);

    List<CreateRebeldeVO> findAll();

    CreateRebeldeVO findById(Long id);

    void updateLocalizacao(UpdateLocalizacaoVO updateLocalizacaoVO);

    void negociar(NegociacaoVO negociacaoVO);

}
