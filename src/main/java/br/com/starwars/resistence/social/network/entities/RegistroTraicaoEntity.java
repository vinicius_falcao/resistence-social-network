package br.com.starwars.resistence.social.network.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"id_relator", "id_reportado"}))
@Data
@NoArgsConstructor
public class RegistroTraicaoEntity {

    public RegistroTraicaoEntity(RebeldeEntity relator, RebeldeEntity reportado) {
        this.relator = relator;
        this.reportado = reportado;
    }

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "id_relator")
    private RebeldeEntity relator;
    @ManyToOne
    @JoinColumn(name = "id_reportado")
    private RebeldeEntity reportado;


}
