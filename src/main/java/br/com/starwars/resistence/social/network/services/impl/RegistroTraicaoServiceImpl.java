package br.com.starwars.resistence.social.network.services.impl;

import br.com.starwars.resistence.social.network.entities.RegistroTraicaoEntity;
import br.com.starwars.resistence.social.network.repositories.RegistroTraicaoRepository;
import br.com.starwars.resistence.social.network.services.RebeldeService;
import br.com.starwars.resistence.social.network.services.RegistroTraicaoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RegistroTraicaoServiceImpl implements RegistroTraicaoService {

    private final RegistroTraicaoRepository registroTraicaoRepository;
    private final RebeldeService rebeldeService;


    @Override
    public void reportarTraidor(Long idRebeldeRelator, Long idRebeldeReportado) {
        final var relator = rebeldeService.findById(idRebeldeRelator);
        final var traidor = rebeldeService.findById(idRebeldeReportado);
        final var registroTraicao = new RegistroTraicaoEntity(relator, traidor);
        registroTraicaoRepository.save(registroTraicao);
        final var totalTraicoes = registroTraicaoRepository.findByReportadoId(traidor.getId()).size();
        if(totalTraicoes >= 3){
            traidor.setTraidor(true);
            rebeldeService.save(traidor);
        }
    }
}
