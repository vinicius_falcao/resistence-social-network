package br.com.starwars.resistence.social.network.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.PERSIST;

@Entity
@Data
public class RebeldeEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    private Integer idade;
    private String genero;
    @Column(columnDefinition = "boolean default false")
    private boolean traidor;
    @OneToOne(cascade = PERSIST)
    private LocalizacaoRebeldeEntity localizacao;
    @OneToMany(cascade = PERSIST, fetch = FetchType.EAGER)
    private List<ItemInventarioEntity> inventario;
}
