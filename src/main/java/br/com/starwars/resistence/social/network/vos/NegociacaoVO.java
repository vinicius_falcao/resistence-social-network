package br.com.starwars.resistence.social.network.vos;

import lombok.Data;

@Data
public class NegociacaoVO {

    private PropostaNegociacaoVO proposta1;
    private PropostaNegociacaoVO proposta2;
}
