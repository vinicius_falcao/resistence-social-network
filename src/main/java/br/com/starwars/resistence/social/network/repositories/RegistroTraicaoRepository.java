package br.com.starwars.resistence.social.network.repositories;

import br.com.starwars.resistence.social.network.entities.RegistroTraicaoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RegistroTraicaoRepository extends JpaRepository<RegistroTraicaoEntity, Long> {

    List<RegistroTraicaoEntity> findByReportadoId(final Long reportadoId);

}
