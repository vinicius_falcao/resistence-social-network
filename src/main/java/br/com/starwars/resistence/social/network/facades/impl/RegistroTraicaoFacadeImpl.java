package br.com.starwars.resistence.social.network.facades.impl;

import br.com.starwars.resistence.social.network.facades.RegistroTraicaoFacade;
import br.com.starwars.resistence.social.network.services.RegistroTraicaoService;
import br.com.starwars.resistence.social.network.vos.ReportarTraidorVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RegistroTraicaoFacadeImpl implements RegistroTraicaoFacade {

    private final RegistroTraicaoService registroTraicaoService;

    @Override
    public void reportarTraidor(ReportarTraidorVO reportarTraidorVO) {
        registroTraicaoService.reportarTraidor(reportarTraidorVO.getIdRebeldeRelator(), reportarTraidorVO.getIdRebeldeReportado());
    }

}
