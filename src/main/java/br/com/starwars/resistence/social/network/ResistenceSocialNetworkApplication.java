package br.com.starwars.resistence.social.network;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResistenceSocialNetworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ResistenceSocialNetworkApplication.class, args);
	}

}
