package br.com.starwars.resistence.social.network.controllers;

import br.com.starwars.resistence.social.network.facades.RegistroTraicaoFacade;
import br.com.starwars.resistence.social.network.vos.ReportarTraidorVO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/registroTraicao")
@RequiredArgsConstructor
public class RegistroTraicaoController {

    private final RegistroTraicaoFacade registroTraicaoFacade;

    @PostMapping
    public void reportarTraicao(@RequestBody final ReportarTraidorVO reportarTraidorVO){
        registroTraicaoFacade.reportarTraidor(reportarTraidorVO);
    }





}
