package br.com.starwars.resistence.social.network.exceptions;

public class TransacaoNaoPermitidaException extends RuntimeException {

    public TransacaoNaoPermitidaException(String message) {
        super(message);
    }
}
