package br.com.starwars.resistence.social.network.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
public class ItemInventarioEntity {

    @Id
    @GeneratedValue
    private Long id;
    private Long quantidade;
    @ManyToOne
    @JoinColumn(name = "id_item")
    private ItemEntity item;
    @ManyToOne
    @JoinColumn(name = "id_rebelde")
    @ToString.Exclude
    private RebeldeEntity rebelde;

}
