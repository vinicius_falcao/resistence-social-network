package br.com.starwars.resistence.social.network.services.impl;

import br.com.starwars.resistence.social.network.entities.LocalizacaoRebeldeEntity;
import br.com.starwars.resistence.social.network.entities.RebeldeEntity;
import br.com.starwars.resistence.social.network.exceptions.RebeldeNotFound;
import br.com.starwars.resistence.social.network.exceptions.TransacaoNaoPermitidaException;
import br.com.starwars.resistence.social.network.repositories.LocalizacaoRebeldeRepository;
import br.com.starwars.resistence.social.network.repositories.RebeldeRepository;
import br.com.starwars.resistence.social.network.services.RebeldeService;
import br.com.starwars.resistence.social.network.vos.ItemInventarioVO;
import br.com.starwars.resistence.social.network.vos.PropostaNegociacaoVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class RebeldeServiceImpl implements RebeldeService {

    private final RebeldeRepository rebeldeRepository;
    private final LocalizacaoRebeldeRepository localizacaoRebeldeRepository;

    @Override
    public RebeldeEntity save(final RebeldeEntity rebelde) {
        return rebeldeRepository.save(rebelde);
    }

    @Override
    public List<RebeldeEntity> findAll() {
        return rebeldeRepository.findAll();
    }

    @Override
    public RebeldeEntity findById(final Long id) {
        return this.rebeldeRepository.findById(id)
                .orElseThrow(RebeldeNotFound::new);
    }

    @Override
    public void updateLocalizacao(final Long idRebelde, final LocalizacaoRebeldeEntity localizacaRebelde) {
        var rebelde = rebeldeRepository.findById(idRebelde)
                .orElseThrow(RebeldeNotFound::new);
        var localizacao = localizacaoRebeldeRepository.save(localizacaRebelde);
        rebelde.setLocalizacao(localizacao);
        rebeldeRepository.save(rebelde);
    }

    @Override
    public void negociar(PropostaNegociacaoVO proposta1, PropostaNegociacaoVO proposta2) {
        if(isTransacaoValida(proposta1, proposta2)){

        }
    }

    private boolean isTransacaoValida(PropostaNegociacaoVO proposta1, PropostaNegociacaoVO proposta2) {
        final var rebelde1 = findById(proposta1.getIdRebelde());
        final var rebelde2 = findById(proposta2.getIdRebelde());
        if(rebelde1.isTraidor() || rebelde2.isTraidor()){
            throw new TransacaoNaoPermitidaException("Pelo menos um dos rebeldes é um traidor");
        }

        if(!inventarioHasItems(rebelde1, proposta1.getItemsInventario())
                || !inventarioHasItems(rebelde2, proposta2.getItemsInventario()) ){
            throw new TransacaoNaoPermitidaException("Pelo menos um dos rebeldes tentou negociar um item que não possui");
        }
        //FIXME codigo incompleto
        return false;
    }

    private boolean inventarioHasItems(RebeldeEntity rebelde1, List<ItemInventarioVO> itemsInventario) {
        return itemsInventario.stream().allMatch(
                item -> rebelde1.getInventario().stream().anyMatch(
                        itemInventario -> itemInventario.getId().equals(item)));
    }
}
