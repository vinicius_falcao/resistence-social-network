package br.com.starwars.resistence.social.network.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Data
public class LocalizacaoRebeldeEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    private Double latitude;
    private Double longitude;

    @ToString.Exclude
    @OneToOne
    @JoinColumn(name = "id_rebelde")
    private RebeldeEntity rebelde;

}
