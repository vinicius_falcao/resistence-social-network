package br.com.starwars.resistence.social.network.controllers;


import br.com.starwars.resistence.social.network.facades.RebeldeFacade;
import br.com.starwars.resistence.social.network.vos.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/rebelde")
@RequiredArgsConstructor
public class RebeldeController {

    private final RebeldeFacade rebeldeFacade;

    @GetMapping
    public List<CreateRebeldeVO> findAll(){
        return rebeldeFacade.findAll();
    }

    @GetMapping("/{id}")
    public CreateRebeldeVO findById(final Long id){
        return this.rebeldeFacade.findById(id);
    }

    @PostMapping
    public CreateRebeldeVO save(@RequestBody final CreateRebeldeVO createRebeldeVO){
        return this.rebeldeFacade.save(createRebeldeVO);
    }

    @PutMapping
    public void updateLocalizacao(@RequestBody final UpdateLocalizacaoVO updateLocalizacaoVO){
        this.rebeldeFacade.updateLocalizacao(updateLocalizacaoVO);
    }

    @PostMapping("/negociacao")
    public void negociar(final NegociacaoVO negociacaoVO){
        this.rebeldeFacade.negociar(negociacaoVO);
    }
}
