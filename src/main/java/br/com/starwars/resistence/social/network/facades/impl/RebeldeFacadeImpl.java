package br.com.starwars.resistence.social.network.facades.impl;

import br.com.starwars.resistence.social.network.entities.ItemEntity;
import br.com.starwars.resistence.social.network.entities.ItemInventarioEntity;
import br.com.starwars.resistence.social.network.entities.LocalizacaoRebeldeEntity;
import br.com.starwars.resistence.social.network.entities.RebeldeEntity;
import br.com.starwars.resistence.social.network.facades.RebeldeFacade;
import br.com.starwars.resistence.social.network.services.RebeldeService;
import br.com.starwars.resistence.social.network.vos.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toUnmodifiableList;

@Component
@RequiredArgsConstructor
public class RebeldeFacadeImpl implements RebeldeFacade {

    private final RebeldeService rebeldeService;

    @Override
    public CreateRebeldeVO save(CreateRebeldeVO createRebeldeVO) {
        return toRebeldeVO(rebeldeService.save(toRebeldeEntity(createRebeldeVO)));
    }

    @Override
    public List<CreateRebeldeVO> findAll() {
        return rebeldeService.findAll().stream().map(this::toRebeldeVO).collect(toList());
    }

    @Override
    public CreateRebeldeVO findById(Long id) {
        return toRebeldeVO(rebeldeService.findById(id));
    }

    @Override
    public void updateLocalizacao(UpdateLocalizacaoVO updateLocalizacaoVO) {
        rebeldeService.updateLocalizacao(updateLocalizacaoVO.getIdRebelde(), toLocalizacaoRebeldeEntity(updateLocalizacaoVO.getLocalizacao()));
    }

    @Override
    public void negociar(NegociacaoVO negociacaoVO) {
        rebeldeService.negociar(negociacaoVO.getProposta1(), negociacaoVO.getProposta2());
    }


    private RebeldeEntity toRebeldeEntity(final CreateRebeldeVO createRebeldeVO){
        final RebeldeEntity rebeldeEntity = new RebeldeEntity();
        rebeldeEntity.setId(createRebeldeVO.getId());
        rebeldeEntity.setIdade(createRebeldeVO.getIdade());
        rebeldeEntity.setGenero(createRebeldeVO.getGenero());
        rebeldeEntity.setNome(createRebeldeVO.getNome());
        final LocalizacaoRebeldeEntity localizacaoRebeldeEntity = toLocalizacaoRebeldeEntity(createRebeldeVO.getLocalizacao());
        localizacaoRebeldeEntity.setRebelde(rebeldeEntity);
        rebeldeEntity.setLocalizacao(localizacaoRebeldeEntity);
        rebeldeEntity.setInventario(
                createRebeldeVO.getInventario()
                        .stream()
                        .map(this::toItemInventarioEntity)
                        .peek(entity -> entity.setRebelde(rebeldeEntity))
                        .collect(toList())
        );

        return rebeldeEntity;
    }

    private LocalizacaoRebeldeEntity toLocalizacaoRebeldeEntity(final LocalizacaoVO localizacaoVO) {
        final LocalizacaoRebeldeEntity localizacaoRebeldeEntity = new LocalizacaoRebeldeEntity();
        localizacaoRebeldeEntity.setLatitude(localizacaoVO.getLatitude());
        localizacaoRebeldeEntity.setLongitude(localizacaoVO.getLongitude());
        localizacaoRebeldeEntity.setNome(localizacaoVO.getNome());
        return localizacaoRebeldeEntity;
    }

    private ItemInventarioEntity toItemInventarioEntity(final ItemInventarioVO itemInventarioVO) {
        final ItemInventarioEntity itemInventarioEntity = new ItemInventarioEntity();
        itemInventarioEntity.setItem(new ItemEntity(itemInventarioVO.getIdItem()));
        itemInventarioEntity.setQuantidade(itemInventarioVO.getQuantidade());
        return itemInventarioEntity;
    }

    private ItemInventarioVO toItemInventarioVO(final ItemInventarioEntity itemInventarioEntity){
        final ItemInventarioVO itemInventarioVO = new ItemInventarioVO();
        itemInventarioVO.setIdItem(itemInventarioEntity.getItem().getId());
        itemInventarioVO.setQuantidade(itemInventarioEntity.getQuantidade());
        return itemInventarioVO;
    }

    private CreateRebeldeVO toRebeldeVO(final RebeldeEntity rebeldeEntity){
        final CreateRebeldeVO createRebeldeVO = new CreateRebeldeVO();
        createRebeldeVO.setId(rebeldeEntity.getId());
        createRebeldeVO.setGenero(rebeldeEntity.getGenero());
        createRebeldeVO.setIdade(rebeldeEntity.getIdade());
        createRebeldeVO.setNome(rebeldeEntity.getNome());
        createRebeldeVO.setTraidor(rebeldeEntity.isTraidor());
        createRebeldeVO.setInventario(
            rebeldeEntity.getInventario()
                    .stream()
                    .map(this::toItemInventarioVO)
                    .collect(toUnmodifiableList())
        );
        createRebeldeVO.setLocalizacao(toLocalizacaoVO(rebeldeEntity.getLocalizacao()));
        return createRebeldeVO;
    }

    private LocalizacaoVO toLocalizacaoVO(LocalizacaoRebeldeEntity localizacaoEntity) {
        final LocalizacaoVO localizacaoVO = new LocalizacaoVO();
        localizacaoVO.setLatitude(localizacaoEntity.getLatitude());
        localizacaoVO.setLongitude(localizacaoEntity.getLongitude());
        localizacaoVO.setNome(localizacaoEntity.getNome());
        return localizacaoVO;
    }


}
