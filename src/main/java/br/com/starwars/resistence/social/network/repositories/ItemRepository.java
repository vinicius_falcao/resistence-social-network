package br.com.starwars.resistence.social.network.repositories;

import br.com.starwars.resistence.social.network.entities.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<ItemEntity, Long> {
}
