package br.com.starwars.resistence.social.network.services;

public interface RegistroTraicaoService {

    void reportarTraidor(Long idRebeldeRelator, Long idRebeldeReportado);
}
