package br.com.starwars.resistence.social.network.repositories;

import br.com.starwars.resistence.social.network.entities.LocalizacaoRebeldeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalizacaoRebeldeRepository extends JpaRepository<LocalizacaoRebeldeEntity, Long> {
}
