##Para rodar a API
```
./mvnw spring-boot:run
```
##Swagger com os endpoints
```
http://localhost:8080/swagger-ui
```
##Executar os testes
```
 ./mvnw test
```